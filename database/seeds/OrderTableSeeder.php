<?php

use Illuminate\Database\Seeder;
use Carbon\carbon;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orderhistories')->insert([
            'id'=>1,
            'orderdate'=>Carbon::now(),
            'productname'=>'Product A',
            'amount'=>3,
            'sum'=>15000,
            'phonenumber_id'=>1

        ]);
        DB::table('orderhistories')->insert([
            'id'=>2,
            'orderdate'=>Carbon::now(),
            'productname'=>'Product B',
            'amount'=>2,
            'sum'=>5000,
            'phonenumber_id'=>1

        ]);
        DB::table('orderhistories')->insert([
            'id'=>3,
            'orderdate'=>Carbon::now(),
            'productname'=>'Product 3',
            'amount'=>5,
            'sum'=>5000,
            'phonenumber_id'=>2

        ]);
        DB::table('orderhistories')->insert([
            'id'=>4,
            'orderdate'=>Carbon::now(),
            'productname'=>'Product 4',
            'amount'=>1,
            'sum'=>25000,
            'phonenumber_id'=>3

        ]);
        DB::table('orderhistories')->insert([
            'id'=>5,
            'orderdate'=>Carbon::now(),
            'productname'=>'Product A',
            'amount'=>5,
            'sum'=>35000,
            'phonenumber_id'=>4

        ]);
        DB::table('orderhistories')->insert([
            'id'=>6,
            'orderdate'=>Carbon::now(),
            'productname'=>'Product B',
            'amount'=>10,
            'sum'=>50000,
            'phonenumber_id'=>3

        ]);
        DB::table('orderhistories')->insert([
            'id'=>7,
            'orderdate'=>Carbon::now(),
            'productname'=>'Product C',
            'amount'=>4,
            'sum'=>2000,
            'phonenumber_id'=>4

        ]);
        DB::table('orderhistories')->insert([
            'id'=>8,
            'orderdate'=>Carbon::now(),
            'productname'=>'Product A',
            'amount'=>1,
            'sum'=>15000,
            'phonenumber_id'=>2

        ]);
    }
}
