<?php

use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("profiles")->insert([
            "id"=>1,
            "first_name"=>"shovit",
            "lastname"=>"Thapa",
            "gender"=>"male",
            "address"=>"Turku",
            "company"=>"TUAS",
            "phonenumber_id"=>1
        ]);
        DB::table("profiles")->insert([
            "id"=>2,
            "first_name"=>"Shova",
            "lastname"=>"Dumrakoti",
            "gender"=>"Female",
            "address"=>"20610, Turku",
            "company"=>"-",
            "phonenumber_id"=>2
        ]);
        DB::table("profiles")->insert([
            "id"=>3,
            "first_name"=>"Hari",
            "lastname"=>"Gurung",
            "gender"=>"male",
            "address"=>"Tampere",
            "company"=>"-",
            "phonenumber_id"=>3
        ]);
        DB::table("profiles")->insert([
            "id"=>4,
            "first_name"=>"Dependra",
            "lastname"=>"Thapa",
            "gender"=>"male",
            "address"=>"Vaasa",
            "company"=>"Vasa University",
            "phonenumber_id"=>4
        ]);
        DB::table("profiles")->insert([
            "id"=>5,
            "first_name"=>"Niti",
            "lastname"=>"Baidya",
            "gender"=>"Female",
            "address"=>"Helsinki",
            "company"=>"Helsinki metropolia",
            "phonenumber_id"=>5
        ]);
        DB::table("profiles")->insert([
            "id"=>6,
            "first_name"=>"Sulav",
            "lastname"=>"Tamang",
            "gender"=>"male",
            "address"=>"Sydney, Australia",
            "company"=>"TUAS",
            "phonenumber_id"=>6
        ]);
        DB::table("profiles")->insert([
            "id"=>7,
            "first_name"=>"Nabina",
            "lastname"=>"KC",
            "gender"=>"Female",
            "address"=>"Sydney, Australia",
            "company"=>"Aaustralian University",
            "phonenumber_id"=>7
        ]);
        DB::table("profiles")->insert([
            "id"=>8,
            "first_name"=>"Aakash",
            "lastname"=>"Neupane",
            "gender"=>"male",
            "address"=>"New York",
            "company"=>"--",
            "phonenumber_id"=>8
        ]);
    }
}
