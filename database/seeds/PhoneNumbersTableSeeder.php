<?php

use Illuminate\Database\Seeder;

class PhoneNumbersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phonenumbers')->insert([
            'id'=>1,
            'number'=> 417037151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>2,
            'number'=> 417947151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>3,
            'number'=> 417947151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>4,
            'number'=> 445947151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>5,
            'number'=> 417947000,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>6,
            'number'=> 417949000,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>7,
            'number'=> 417947191,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>8,
            'number'=> 417911151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>9,
            'number'=> 419021567,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>10,
            'number'=> 417947151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>11,
            'number'=> 417997151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>12,
            'number'=> 410000051,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>13,
            'number'=>56787151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>14,
            'number'=> 417922222,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>15,
            'number'=> 567947151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>16,
            'number'=> 411234151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>17,
            'number'=> 417666151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>18,
            'number'=> 411234551,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>19,
            'number'=> 557947151,
            'extension'=>358
        ]);
        DB::table('phonenumbers')->insert([
            'id'=>20,
            'number'=> 497947151,
            'extension'=>358
        ]);
    }
}
