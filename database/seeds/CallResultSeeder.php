<?php

use Illuminate\Database\Seeder;

class CallResultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('callresults')->insert([
            'id'=>1,
            'result'=>"sold"
        ]);
        DB::table('callresults')->insert([
            'id'=>2,
            'result'=>"not interested"
        ]);
        DB::table('callresults')->insert([
            'id'=>3,
            'result'=>"no answer"
        ]);
    }
}
