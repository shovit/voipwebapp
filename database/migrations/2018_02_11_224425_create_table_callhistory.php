<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCallhistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('callhistories', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('phonenumber_id')->unsigned();
            $table->integer('callresult_id')->unsigned();            
            $table->timestamps();

            $table->foreign('phonenumber_id')->references('id')->on('phonenumbers');
            $table->foreign('callresult_id')->references('id')->on('callresults');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('callhistories');
    }
}
