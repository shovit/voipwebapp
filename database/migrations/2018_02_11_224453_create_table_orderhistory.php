<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderhistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderhistories', function (Blueprint $table) {
            $table->increments('id');
            $table->date('orderdate');
            $table->string('productname');
            $table->integer('amount');
            $table->double('sum');
            $table->integer('phonenumber_id')->unsigned();
            $table->timestamps();

            $table->foreign('phonenumber_id')->references('id')->on('phonenumbers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderhistories');
    }
}
