<?php

namespace App\Http\Controllers;
use App\PhoneNumber;
use App\Profile;


use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('layout.dashboard')->with("phoneNumbers", PhoneNumber::paginate(8));
       
    }
    
}
