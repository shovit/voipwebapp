<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PhoneNumber;
use App\Profile;
use App\OrderHistory;
use Illuminate\Support\Facades\DB;

class PhoneController extends Controller
{
    public function Phone($id){

       return view('layout/profile')
      ->with("phoneNumber",PhoneNumber::Find($id))
      ->with("profile", PhoneNumber::find($id)->Profile)
      ->with("callHistory",DB::table("callhistories")->where("phonenumber_id",$id)->join("callresults","callresults.id","=","callhistories.callresult_id")->get())
      ->with("orderHistory",PhoneNumber::Find($id)->OrderHistory);
       
    }
}
