<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model
{
    protected $table="phonenumbers";

    public function Profile(){
        return $this->hasOne("App\Profile","phonenumber_id");
    }
    public function CallHistory(){
        return $this->hasMany("App\CallResult","phonenumber_id");
    }
    public function OrderHistory(){
        return $this->hasMany("App\OrderHistory","phonenumber_id");
    }
}
