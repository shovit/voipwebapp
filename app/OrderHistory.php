<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $table="orderhistories";
    public function OrderHistoryNumber(){
        return $this->belongsTo("App\PhoneNumber");
    }
}
