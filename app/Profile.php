<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table="profiles";

    public function PhoneNumber(){
        return $this->belongsTo("App\PhoneNumber");
    }
}
