<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallHistory extends Model
{
    protected $table="callhistories";
    public function PhoneNumber(){
        return $this->belongTo("App\PhoneNumber");
    }
}
