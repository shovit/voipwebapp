<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landingPage');
});

Route::get(
    'home', ['as' => 'home', 'uses'=>"HomeController@index" ]
);

Route::post(
    'token',
    ['uses' => 'TokenController@newToken', 'as' => 'new-token']
);
Route::get(
    '/dashboard',
    ['uses' => 'DashboardController@dashboard', 'as' => 'dashboard']
);
Route::post(
    '/ticket',
    ['uses' => 'TicketController@newTicket', 'as' => 'new-ticket']
);
Route::post(
    '/support/call',
    ['uses' => 'CallController@newCall', 'as' => 'new-call']
);
Route::get('/home/{id}', 'PhoneController@Phone');

/*broad cast resources*/
Route::get(
    '/conference',
    ['uses' => 'ConferenceController@index', 'as' => 'conference-index']
);
Route::get(
    '/conference/join',
    ['uses' => 'ConferenceController@showJoin', 'as' => 'conference-join']
);
Route::get(
    '/conference/connect',
    ['uses' => 'ConferenceController@showConnect', 'as' => 'conference-connect']
);

//Broadcast related routes
Route::get(
    '/broadcast',
    ['uses' => 'ConferenceBroadCastController@index', 'as' => 'broadcast-index']
);

Route::post(
    '/broadcast/send',
    ['uses' => 'ConferenceBroadBroadCastController@send', 'as' => 'broadcast-send']
);

Route::post(
    '/broadcast/play',
    ['uses' => 'ConferenceBroadCastController@showPlay', 'as' => 'broadcast-play']
);

//Recording related routes
Route::get(
    '/recordings',
    ['uses' => 'ConferenceVoiceRecordController@index', 'as' => 'recording-index']
);

Route::post(
    '/recording/create',
    ['uses' => 'ConferenceVoiceRecordController@create', 'as' => 'recording-create']
);

Route::post(
    '/recording/record',
    ['uses' => 'ConferenceVoiceRecordController@showRecord', 'as' => 'recording-record']
);

Route::get(
    '/recording/hangup',
    ['uses' => 'ConferenceVoiceRecordController@showHangup', 'as' => 'recording-hangup']
);