<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>LeadDesk</title>

       <!-- Fonts -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">        
        <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
              
    <body>
    
    <!--BEM-Model-->
    <header class="header">
        @if($errors->any())
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
        @endif
        <nav class="nav">
            <div class="nav__logo">
                <img class="nav__logo--img" src="{{URL::asset('img/leadesk.png')}}">
            </div>
            <div class="nav__login">
                <a href="{{URL::asset('index.php/home')}}"><button class="nav__login--btn">               
               Dashboard</button></a>
            </div>
        </nav>
        <div class="header__text-box">
            <div class="header__primary">
            <h1>
                <span class="header__primary--title">Customer Service Matters !</span>
                <span class="header__primary--subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Aenean aliquet ante diam, eu sagittis augue consequat at.Ut sed leo eu ipsum lacinia congue non id sem. Mauris eget congue mi, nec efficitur risus. Praesent.</h2>
            </h1>
            <div class="header__primary--statistic"></div>
                <div class="header__statistic--happy">
                    <img src="{{URL::asset('img/happy_customers.png')}}"><br>
                    <span class="statistic-figure">6000</span>
                </div>
                <div class="header__statistic--handshake">
                    <img src="{{URL::asset('img/handshake.png')}}"><br>
                    <span class="statistic-figure">100000</span>
                </div>
                <div class="header__statistic--callpeople">
                    <img src="{{URL::asset('img/callpeople.png')}}"><br>
                    <span class="statistic-figure">60000000</span>
                </div>
            </div>            
        </div>
        
    </header>
    <div class="header__bottom">
            <p class="header__bottom-text">&#169;Copyright to Shovit Thapa, Turku, Finland</p>
    </div> 

   
    </body>
</html>
