@extends("layout/master")
@section("page-content")
<section class="content__phone">              
    <div class="table">
                <table class="phone__list--table">
                    <caption>                        
                        <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Etiam vestibulum ornare ullamcorper. Ut tempor velit sit amet laoreet maximus. 
                                Fusce eu est sed ex sagittis volutpat. Aliquam hendrerit id nibh et sodales. 
                                Nunc efficitur lacus ut nunc volutpat posuere. Pellentesque gravida interdum dictum.
                                 Vivamus maximus lacinia nunc, sit amet auctor.</p>  
                    </caption>

                    <thead>
                        <td>Extension</td>
                        <td>Phone Number</td>
                        <td>Date Added</td>
                    </thead>
                    <tbody>

                    @foreach($phoneNumbers as $number)
                    <tr class="table__row--clickable">
                    
                            <td><a href="{{url('home/'.$number->id)}}">(+{{$number->extension}}) </a></td>
                            <td><a href="{{url('home/'.$number->id)}}">{{$number->number}}</a></td>
                            <td> <a href="{{url('home/'.$number->id)}}">{{date('d F Y', strtotime($number->created_at))}}</a></td>
                            </a>
                    </tr>
                    @endforeach                       
                    </tbody>
                </table>
            <div class="paginate">
            {!! $phoneNumbers->links()!!}
            </div>
    </div>
</section>
@endsection
