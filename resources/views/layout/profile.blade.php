@extends("layout/master")
@section("page-content")
<section class="content__profile">
              
              <div class="user-profile">         

              <div class="order-form">
                    <h3>CUSTOMER ORDER FORM</h3>
                     <div>
                        <p class="level">Select product</p>
                        <select name="product" class="input">
                            <option>Product A</option>
                            <option>Product B</option>
                            <option>Product C</option>
                            <option>Product D</option>
                            <option>Product E</option>
                            <option>Product F</option>
                            </select>
                        </div>

                        <div>
                        <p class="level">Amount</p>
                            <input type="number" min=0 name="amount" class="input"></input>
                        </div>
                        <div>
                        <p class="level">Billing Address</p>
                            <input type="text" min=0 name="amount" class="input"></input>
                        </div>
                        <div>
                        <p class="level">Special Request</p>
                           <textarea rows="5" cols="34" placeholder="Any special request from customer.."></textarea> 
                        </div>
                        <div class="confirm-btn">
                            <Button>Confirm</Button><Button onclick="CloseForm()">Close</Button>
                        </div>   
                </div>


                 <div class="phone-dialer">
                    <div class="avatar">
                    <img src="{{URL::asset('img/user.png')}}">                   
                    </div>
                    <div class="call-status">
                    Calliing...
                    </div>
                    <div class="profile__phonenumber">
                    (+{{$phoneNumber->extension}}){{$phoneNumber->number}}
                    </div>
                    <div class="phone__buttons">
                        <span class="phone__btn phone-down" onclick="hangUp('+{{$phoneNumber->extension}}{{$phoneNumber->number}}')"><i class="fas fa-phone"></i></span>
                        <span class="phone__btn phone-order" onclick="makeNewOrder()"><i class="fab fa-wpforms"></i></span>
                    </div>
                </div> 
               

                  <div class="avatar">
                    <img src="{{URL::asset('img/user.png')}}">
                  </div>
                  <div class="profile__phonenumber">
                    
                    (+{{$phoneNumber->extension}}){{$phoneNumber->number}}
                    <br>
                    <Button class="dial" onclick="MakeCustomerCall('+{{$phoneNumber->extension}}{{$phoneNumber->number}}')"><i class="fas fa-phone"></i> Dial</Button>
                  </div>
                  <div class="profile__info">
                      <ul>
                          <li>{{$profile->first_name}} {{ $profile->lastname}}</li>
                          <li>Gender :  {{$profile->gender}}</li>
                          <li>Addresss : {{$profile->address}}</li>
                          <li>Company : {{$profile->company}}</li>
                       </ul>
                  </div>
                  <div class="confirm-btn">
                           <Button onclick="makeNewOrder()">New order</Button>
                    </div>   
              </div>


              <div class="call__history">
                    <div class="table">
                            <table class="call__history--table">
                                <caption>                        
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                            Etiam vestibulum ornare ullamcorper. Ut tempor velit sit amet laoreet maximus. 
                                            Fusce eu est sed ex sagittis volutpat. Aliquam hendrerit id nibh et sodales. 
                                            Nunc efficitur lacus ut nunc volutpat posuere. Pellentesque gravida interdum dictum.
                                             Vivamus maximus lacinia nunc, sit amet auctor.</p>  
                                </caption>
           
                                <thead>
                                    <td>Call History</td>
                                    <td>Call Status</td>                                    
                                </thead>
                                <tbody>
                                    @if(count($callHistory)>0)
                                    @foreach($callHistory as $history)
                                   
                                   <tr><td>{{date('d F Y', strtotime($history->date))}} ,  {{ date('G:i', strtotime($history->date)) }}</td>
                                   @if($history->result=="sold")
                                   
                                   <td><Button class="sold"> <i class="fas fa-check"></i> {{$history->result}}</Button></td>
                                   
                                   @elseif($history->result=="not interested")
                                   
                                   <td> <Button class="not--interested"><i class="fas fa-times"></i> {{$history->result}} </Button></td>
                                  
                                   @elseif($history->result=="no answer")
                                   
                                   <td> <Button class="no--answer"><i class="fas fa-exclamation-triangle"></i> {{$history->result}}</Button></td>
                                   
                                   @endif
                                   </tr>
                                   @endforeach
                                   @else
                                   <tr>
                                       <td>No any call </td>
                                       <td>No any call </td>
                                    </tr>
                                    @endif
                                   
                                                                      
                                </tbody>
                            </table>
                    </div>

                <!--order history-->

                    <div class="table">
                            <table class="call__history--table">
                                <caption>                        
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                            Etiam vestibulum ornare ullamcorper. Ut tempor velit sit amet laoreet maximus. 
                                            </p>  
                                </caption>
           
                              
                                <thead>
                                    <td>Order Date</td>
                                    <td>Product</td>     
                                    <td>Amount</td>   
                                    <td>Total Sum</td>                            
                                </thead>
                                <tbody>
                                    @if(count($orderHistory)>0)
                                    @foreach($orderHistory as $order)
                                   <tr>
                                       <td>
                                       {{$order->orderdate}}
                                       </td>
                                       <td>
                                       {{$order->productname}}
                                       </td>
                                       <td>
                                       {{$order->amount}}
                                       </td>
                                       <td>
                                       {{$order->sum}}
                                       </td>
                                   </tr>
                                   @endforeach
                                   @else
                                   <tr>
                                       <td>No any orders yet</td><td>No any orders yet</td><td>No any orders yet</td><td>No any orders yet</td>
                                
                                </tr>
                                   
                                    @endif                                  
                                                                      
                                </tbody>
                            </table>
                    </div>
                </div>
            </section>
         
@endsection

@section("scripts")
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="//static.twilio.com/libs/twiliojs/1.2/twilio.min.js"></script>
    <script src="{{ URL::asset('js/PhoneCall.js') }}"></script>
@endsection