<!doctype html>
<html lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LeadDesk</title>

        <!-- Fonts -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">        
        <link rel="stylesheet" href="{{URL::asset('css/dashboard.css')}}">
    </head>
    <body>

        
    <div class="body-wrapper">
            <nav class="nav">
                <ul>
                    <li><a href="{{URL::asset('index.php/home')}}" title="Home"><i class="fas fa-home"></i></a></li>
                    <li><i class="fas fa-briefcase"></i></li>
                    <li><i class="fab fa-elementor"></i></li>
                    <li><i class="fas fa-chart-bar"></i></li>
                    <li><i class="far fa-calendar-alt"></i></li>
                    <li><i class="far fa-folder-open"></i></li>
                    <li><i class="fas fa-map-marker-alt"></i></li>
                    <li><i class="fas fa-paperclip"></i></li>
                    <li><i class="fas fa-align-right"></i></li>
                    <li><i class="fas fa-eraser"></i></li>
                    <li><i class="fas fa-exclamation-triangle"></i></li>
                    <li><i class="fab fa-gg-circle"></i></li>
                    <li><i class="fab fa-servicestack"></i></li>
                    <li><i class="fas fa-trash-alt"></i></li>
                </ul>
            </nav>

            @yield("page-content")
            
        </div>

    @yield("scripts");
    
    </body>
    </html>